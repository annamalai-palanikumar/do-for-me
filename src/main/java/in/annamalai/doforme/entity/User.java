package in.annamalai.doforme.entity;

import org.jboss.aerogear.security.otp.api.Base32;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Setter
@Getter
public class User {

    @Id
    private Long id;
    private boolean isUsing2FA;
    private String secret;

    public User() {
        super();
        this.secret = Base32.random();
    }
}