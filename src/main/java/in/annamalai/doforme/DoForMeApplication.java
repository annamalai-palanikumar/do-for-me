package in.annamalai.doforme;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DoForMeApplication {

	public static void main(String[] args) {
		SpringApplication.run(DoForMeApplication.class, args);
	}

}
